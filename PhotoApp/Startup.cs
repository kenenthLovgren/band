﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PhotoApp.Startup))]
namespace PhotoApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
