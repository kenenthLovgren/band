﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    public class PhotoSharingInitializer : DropCreateDatabaseAlways<PhotoShareContex>
    {
        protected override void Seed(PhotoShareContex context)
        {
            List<Photo> photoList = new List<Photo>()
            {
                new Photo()
                {
                    Title = "Test Photo",
                    Description = "A test photo",
                    PhotoFile = getFileBytes("\\Images\\flower.jpg"),
                    Mimetype = "Image/jpeg",
                    CreatedDate = DateTime.Now

                }

            };

            foreach (var photo in photoList)
            {
                context.Photos.Add(photo);
            }
            context.SaveChanges();

            List<Comment> comments = new List<Comment>()
            {
                new Comment()
                {
                    Id = 1,
                    UserName = "kenneth",
                    Subject = "Test comment",
                    Body = "This comment should appear in photo 1",

                }
            };

            foreach (var comment in comments)
            {
                context.Comments.Add(comment);
            }
            context.SaveChanges();
        }

        

        //This gets a byte array for a file at the path specified
        //The path is relative to the route of the web site
        //It is used to seed images

        public byte[] getFileBytes(string path)
        {
            FileStream fileOnDisk = new FileStream(HttpRuntime.AppDomainAppPath + path, FileMode.Open);
            byte[] fileBytes;
            using (BinaryReader br = new BinaryReader(fileOnDisk))
            {
                fileBytes = br.ReadBytes((int)fileOnDisk.Length);
            }
            return fileBytes;
        }
    }
}