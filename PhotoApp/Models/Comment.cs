﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PhotoApp.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string PhotoId { get; set; }
        [Required]
        [StringLength(250)]
        public string UserName { get; set; }
        [Required]
        [StringLength(500)]
        public string Subject { get; set; }
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }
        public virtual Photo Photo { get; set; }
    }
}