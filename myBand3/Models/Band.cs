﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace myBand3.Models
{
    public class Band
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string BandName { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EstablishedDate  { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DataType(DataType.Url)]
        public string OfficialSite { get; set; }
        [HiddenInput]
        public string Owner { get; set; }

        public virtual ICollection<Album> Album { get; set; }
    }
}