﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;

namespace myBand3.Models
{
    public class Album
    {
        public int Id { get; set; }
        public int BandId { get; set; }
        [Required]
        [StringLength(100)]
        public string AlbumName { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Published { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        //TODO reject rules
        public string Description { get; set; }
        [HiddenInput]
        public string User { get; set; }

        public virtual Band Band { get; set; }

    }
}