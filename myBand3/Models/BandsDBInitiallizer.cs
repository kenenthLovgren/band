﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace myBand3.Models
{
    public class BandsDBInitiallizer : DropCreateDatabaseAlways<BandsDB>
    {
        protected override void Seed(BandsDB context)
        {
            List<Band> bandsList = new List<Band>()
            {
                new Band()
                {
                    BandName = "The Rolling Stones",
                    Description = "The Rolling Stones are an English band...",
                    EstablishedDate = new DateTime( 1962,01,01),
                    OfficialSite = "Http://www.rollingstone.com",
                    Owner = "kenneth"
                }
            };

            foreach (Band band in bandsList)
            {
                context.Bands.Add(band);
            }

            context.SaveChanges();
        }
    }
}